package com.restemp.bean;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
public class AddressBean {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer empID;
	private Integer street;
	private String area;
	private String state;
	private Integer zipcode;
}
