package com.restemp.repository;

import org.springframework.boot.autoconfigure.amqp.RabbitConnectionDetails.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.restemp.bean.AddressBean;

@Repository
public interface AddressRepository extends JpaRepository<AddressBean,Integer>{
	String query="select ea.empid,ea.street,ea.area,ea.state,ea.zipcode \r\n"
			+ "FROM address_bean ea \r\n"
			+ "join employee_bean e on e.id=ea.empid where ea.empid=:employeeID";
	
	@Query(nativeQuery = true,value=query)
	Address findAddressByEmployeeID(@Param("employeeID") Integer employeeID);
}
