package com.restemp.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.restemp.bean.AddressBean;
import com.restemp.repository.AddressRepository;
import com.restemp.response.AddressResponse;


@Service
public class AddressService {

	@Autowired
	AddressRepository repository;
	
	@Autowired
	ModelMapper mapper;
	
	@Autowired
	AddressResponse response;
	
	public void saveAddress(AddressBean empObject) {
		System.out.println(empObject);
	repository.save(empObject);	
	}
	
	public AddressResponse getAddressByID(Integer ID) {
		AddressBean bean=repository.findById(ID).get();
		response = mapper.map(bean,AddressResponse.class);
		return response;
	}
}
