package com.restemp.response;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@Setter
@Getter
public class AddressResponse {
	private Integer empID;
	private Integer street;
	private String area;
	private String state;
	private Integer zipcode;
	
}
