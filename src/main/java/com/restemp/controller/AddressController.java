package com.restemp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.restemp.bean.AddressBean;
import com.restemp.response.AddressResponse;
import com.restemp.service.AddressService;


@RestController
//@RequestMapping("/adr")
public class AddressController {

	@Autowired
	AddressService adrService;
	
	@PostMapping("/save")
	public ResponseEntity<String> postMethodName(@RequestBody AddressBean address) {
		//TODO: process POST request
		adrService.saveAddress(address);
		return new ResponseEntity<>("Address Saved",HttpStatus.OK);
	}
	
	@GetMapping("/get/{id}")
	public ResponseEntity<AddressResponse> getAddressByID(@PathVariable(value="id") int id){
		return ResponseEntity.status(HttpStatus.OK).body(adrService.getAddressByID(id));
	}
	
}
